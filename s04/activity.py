# rows = int(input("Enter the number of rows: "))
# columns = int(input("Enter the number of columns: "))
# # print(rows)
# # print(columns)
# print("Input the values for first matrix ")
# matrix_one = []
# print("Give entries row wise")
# for i in range(rows):
# 	r = []
# 	for j in range(columns):
# 		r.append(int(input()))
# 		# input("matrix_one[i][j] = ")
# 	matrix_one.append(r)

# # matrix_two = array([])
# # matrix_sum = add(matrix_one, matrix_two)
# print(matrix_one)

# matrix_two = []

# for i in range(rows):
# 	x = []
# 	for j in range(columns):
# 		x.append(int(input()))
# 		# input("matrix_one[i][j] = ")
# 	matrix_two.append(x)
# print(matrix_two)

# #To display in matrix form

# for i in range(rows):
# 	for j in range(columns):
# 		print(matrix_one[i][j], end=" ")
# 	print()

# #sum
# matrix_sum = []
# # matrix_sum = (matrix_one + matrix_two)
# for i in range(len(matrix_one)):
# 	for j in range(len(matrix_two[0])):
# 		matrix_sum[i][j] = matrix_one[i][j] + matrix_two[i][j]
# # print(matrix_sum)
# for r in matrix_sum:
# 	print("Matrix_sum ",r)

# capstone quiz (below question code to check here)
# numbersList = [10,20,30,40]
# numbersList[4] = "100"
# print(numbersList)

# Stack = [10,20,30,40]
# Stack.push(70)
# Stack.push(100)
# Stack.pop()
# len(Stack)
# peek(Stack)

# 2
r = int(input(f'Enter the number of rows --> '))
c = int(input(f'Enter the number of columns --> '))

# 3
from numpy import *
matrixOne = [[0 for _ in range(c)] for _ in range(r)]
matrixTwo = [[0 for _ in range(c)] for _ in range(r)]
matrixSum = [[0 for _ in range(c)] for _ in range(r)]

#4
print("Input the values for First matrix")
for i in range(r):
	for j in range(c):
		matrixOne[i][j] = int(input(f'matrixOne[{i}][{j}]= '))

print("Input the values for Second matrix")
for i in range(r):
	for j in range(c):
		matrixTwo[i][j] = int(input(f'matrixTwo[{i}][{j}]= '))


print("******Display Matrix 1******")
for i in range (r):
	print(matrixOne[i])

print("******Display Matrix 2******")
for i in range (r):
	print(matrixTwo[i])


# 5
for i in range(r):
	for j in range(c):
		matrixSum[i][j] = matrixOne[i][j] + matrixTwo[i][j]
print("Sum of Two matrix")
for i in range (r):
	print(matrixSum[i])